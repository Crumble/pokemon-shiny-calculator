
import React, { Component } from 'react';

import {Tooltip, IconButton, Grid, Typography, FormControl, FormHelperText, Input, InputLabel, FormControlLabel, FormGroup, Checkbox, AppBar, Toolbar, Chip, BottomNavigation, BottomNavigationAction} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import {Settings, PanoramaFishEye, Timer, ScatterPlot} from '@material-ui/icons';

import { ColumnChart } from 'react-chartkick';


import pokeball from '../pokeball.png';


const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    grow: {
        flexGrow: 1,
    },
    header: {
        textAlign: 'center',
        backgroundColor: '#ff0000',
        marginBottom: 20
    },
    footer: {
        position: 'fixed',
        bottom: 0,
        textAlign: 'center',
        backgroundColor: '#ff0000',
        width: '100%',
        height: 60
    },
    toggleSection:{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: 20
    },
    section: {
        marginBottom: 10
    },
    rowSection:{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        flexWrap: 'wrap'
    },
    formControl: {
        margin: theme.spacing.unit,
    },
    navBar: {
        display: 'flex',
        flexDirection: 'center'
    },
    chip: {
        margin: theme.spacing.unit,
    },
    button: {
        backgroundColor: '#ff0000',
        color: '#fff',

    },
    pokeball: {
        width: 128,
        height: 128,
    },
    pokeballHolder: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        position: 'fixed',
        bottom: 60,
        textAlign: 'center',
        width: '100%',
    },
    graphHolder:{
        height: 60,
        width: '100%'

    }

});

class App extends Component {
    constructor(props){
        super(props);
        this.state = {
            value: 0,
            hasLure: false,
            hasCharm: false,
            disableCatchCombo: false,
            trialCount: 100,
            trials: [{}],
            graphData: [],
            mostCaught: 0,
            leastCaught: 10000,
            pokemonCaught: 0,
            shinyChance: 4096,
            gotPokemon: false,
            trialRunning: false,

            /*Manual stats*/
            manualPokemonCaught: 0,
            manualBest: 0,
            manualWorst: 0,
            manualShinyChance: 4096,
            manualGotShiny: false,
            manualShinyCaughtAt: 0,
            manualTrials: [],
            manualGraphData: [],

            /*Auto Stats*/
            autoPokemonCaught: 0,
            autoBest: 0,
            autoWorst: 0,
            autoShinyChance: 4096,
            autoGotShiny: false,
            autoShinyCaughtAt: 0,
            autoTrials: [],
            autoGraphData: [],
        };
    }

    handleChange = (e) => {
        this.setState( { [e.currentTarget.id]: e.target.checked } );
    };

    handleNavChange = (event, value) => {
        this.setState({ value });
    };

    changeNumber = (e) => {
        if(e.target.value <= 100){
            if(e.target.value >= 2){
                this.setState( { [e.currentTarget.id]: e.target.value } );
            } else {
                this.setState( { [e.currentTarget.id]: 2 } );
            }
        } else {
            this.setState( { [e.currentTarget.id]: 100 } );
        }

    };

    getRandomFloat = (min, max) => {
        let rand = Math.random()*(max-min) + min;
        let power = Math.pow(10, 2); //2 decimal places accuracy
        return Math.floor(rand*power) / power;
    };

    getAveragePokemonCaught = arr => {return Math.floor(arr.reduce( ( p, c ) => p + c, 0 ) / arr.length) };

    getShinyChance = (pokemonCaught) => {
        const { hasLure, hasCharm, disableCatchCombo } = this.state;

        if(disableCatchCombo){
            if(hasCharm && hasLure){
                return 1024;
            }
            else if(hasCharm){
                return 1365.3;
            }
            else if (hasLure){
                return 2048;
            }
            else {
                return 4096;
            }
        }

        if(pokemonCaught <= 10){
            if(hasCharm && hasLure){
                return 1024;
            }
            else if(hasCharm){
                return 1365.3;
            }
            else if (hasLure){
                return 2048;
            }
            else {
                return 4096;
            }
        }
        else if(pokemonCaught <= 20){
            if(hasCharm && hasLure){
                return 585.14;
            }
            else if(hasCharm){
                return 682.6;
            }
            else if (hasLure){
                return 819.20;
            }
            else {
                return 1024;
            }

        }
        else if(pokemonCaught <= 30){
            if(hasCharm && hasLure){
                return 372.36;
            }
            else if(hasCharm){
                return 409.60;
            }
            else if (hasLure){
                return 455.10;
            }
            else {
                return 512;
            }

        }
        else {
            if(hasCharm && hasLure){
                return 273.07;
            }
            else if(hasCharm){
                return 292.57;
            }
            else if (hasLure){
                return 315.08;
            }
            else {
                return 341.3;
            }
        }
    };

    manualRun = () => {

        //disable the button
        this.setState({
            trialRunning: true,
        });

        const {manualPokemonCaught} = this.state;
        let shinyChance = this.getShinyChance(manualPokemonCaught);
        let newSpawn = this.getRandomFloat(0,(shinyChance));

        //wait 20ms and turn button back on and show results
        //we need the delay to ensure the 2 set states don't get batched together
        setTimeout(() => {
            if(newSpawn <= 1){
                let shinyCaughtAt = manualPokemonCaught;

                this.setState({
                    manualPokemonCaught: 0,
                    manualShinyChance: this.getShinyChance(0),
                    manualGotShiny: true,
                    trialRunning: false,
                    manualShinyCaughtAt: shinyCaughtAt,
                    manualBest: ((this.state.manualBest === 0 ||  manualPokemonCaught < this.state.manualBest) ? manualPokemonCaught : this.state.manualBest),
                    manualWorst: (manualPokemonCaught > this.state.manualWorst ? manualPokemonCaught : this.state.manualWorst),
                    manualTrials: [...this.state.manualTrials, manualPokemonCaught],
                    manualGraphData: [...this.state.manualGraphData, ["Run " + (this.state.manualTrials.length + 1), manualPokemonCaught]]
                });

                setTimeout(() => {
                    this.setState({
                        manualGotShiny: false,
                    });
                }, 3000);

            } else {
                this.setState({
                    manualPokemonCaught: manualPokemonCaught + 1,
                    manualShinyChance: shinyChance,
                    trialRunning: false,
                    gotPokemon: false,
                })

            }
        }, 20);



    };

    runTrial = () => {
        let shinies = 0;
        let pokemonCaught = 0;

        while (shinies === 0){
            let newSpawn = this.getRandomFloat(0,(this.getShinyChance(pokemonCaught)));
            pokemonCaught++;
            if(newSpawn <= 1){
                shinies++;
            }
        }

        return pokemonCaught;
    };

    singleRun = () => {

        this.setState({
            trialRunning: true
        });

        let autoPokemonCaught = 0;

        let spawnPokemon = setInterval(() => {

            let shinyChance = this.getShinyChance(autoPokemonCaught);
            let newSpawn = this.getRandomFloat(0,(shinyChance));
            autoPokemonCaught++;


            this.setState({
                autoPokemonCaught: autoPokemonCaught,
                autoShinyChance: shinyChance
            });

            if(newSpawn <= 1){

                let shinyCaughtAt = autoPokemonCaught;

                clearInterval(spawnPokemon);

                this.setState({
                    autoPokemonCaught: 0,
                    autoShinyChance: this.getShinyChance(0),
                    autoGotShiny: true,
                    trialRunning: false,
                    autoShinyCaughtAt: shinyCaughtAt,
                    autoBest: ((this.state.autoBest === 0 ||  autoPokemonCaught < this.state.autoBest) ? autoPokemonCaught : this.state.autoBest),
                    autoWorst: (autoPokemonCaught > this.state.autoWorst ? autoPokemonCaught : this.state.autoWorst),
                    autoTrials: [...this.state.autoTrials, autoPokemonCaught],
                    autoGraphData: [...this.state.autoGraphData, ["Run " + (this.state.autoTrials.length + 1), autoPokemonCaught]]
                });

                setTimeout(() => {
                    this.setState({
                        autoGotShiny: false,
                    });
                }, 3000);
            }
        }, 20);
    };

    multiRun = () => {
        const {trialCount} = this.state;

        //clear results
        this.setState({
            mostCaught: 0,
            leastCaught: 10000,
            trials: [],
            graphData: [],
            trialRunning: true
        });

        for (let i = 0; i < trialCount; i++){
            // eslint-disable-next-line
            setTimeout(() => {
                let result = this.runTrial();
                this.setState({
                    mostCaught: (result > this.state.mostCaught ? result : this.state.mostCaught),
                    leastCaught: (result < this.state.leastCaught ? result : this.state.leastCaught),
                    trials: [...this.state.trials, result],
                    graphData: [...this.state.graphData, ["Run " + (i + 1), result]]
                });

                if(i === (trialCount - 1)){
                    this.setState({
                        trialRunning: false
                    })
                }

            }, 20);


        }

    };



    startSim = () => {
        const {value} = this.state;
        //manual mode
        if(value === 0){
            //manual mode
            this.manualRun();
        }
        //auto mode
        else if (value === 1){
            this.singleRun();
        }
        //multi mode
        else{
            this.multiRun();
        }
    };

    render() {

        const {classes} = this.props;
        const {value} = this.state;

        return (

            <div className={classes.root}>

                <AppBar position="static" className={classes.header}>
                    <Toolbar className={classes.toolBar}>
                        <Typography variant="headline" color="inherit" className={classes.grow}>
                            Pokemon Shiny Calculator
                        </Typography>
                    </Toolbar>
                </AppBar>

                {/*Manual Session*/}
                {value === 0 && (
                    <Grid container alignContent={'center'} direction={'column'} className={classes.section}>
                        <Grid item xs={12} className={classes.rowSection}>

                            <Tooltip title="Best Score so far in manual mode!" placement="top">
                                <Chip label={`Best: ${this.state.manualBest}`} color="primary" className={classes.chip} variant="outlined" />
                            </Tooltip>

                            <Tooltip title="Pokemon caught so far this run!" placement="top">
                                <Chip label={`Pokemon Caught: ${this.state.manualPokemonCaught}`} className={classes.chip} variant="outlined" />
                            </Tooltip>

                            <Tooltip title="Average Pokemon Spawned to get a shiny over all runs in manual mode" placement="top">
                                <Chip label={`Average: ${this.getAveragePokemonCaught(this.state.manualTrials) || 0}`} className={classes.chip} variant="outlined" />
                            </Tooltip>

                            <Tooltip title="Shiny chance, taking into considering lure, charm and catch combo" placement="top">
                                <Chip label={`Shiny Chance:  ${this.state.manualShinyChance}`} className={classes.chip} variant="outlined" />
                            </Tooltip>

                            <Tooltip title="Worst run so far, eg the most amount of pokemon it has taken." placement="top">
                                <Chip label={`Worst Run: ${this.state.manualWorst}`} color="secondary" className={classes.chip} variant="outlined" />
                            </Tooltip>
                        </Grid>

                        <Grid item xs={12} className={classes.graphHolder}>
                            <ColumnChart label="Pokemon Caught" id={'Pokemon Caught Until Shiny'} data={this.state.manualGraphData} min={0} max={null} legend={'bottom'}/>
                        </Grid>

                        {this.state.manualGotShiny && (
                            <Grid item xs={12}>
                                <Typography variant={'display3'} align={'center'}>
                                    Got Shiny!
                                </Typography>

                                <Typography variant={'title'} gutterBottom={true} align={'center'}>
                                    Caught after {this.state.manualShinyCaughtAt} pokemon spawned!
                                </Typography>
                            </Grid>
                        )}



                    </Grid>
                )}

                {/*Single Session*/}
                {value === 1 && (
                    <Grid container alignContent={'center'} direction={'column'} className={classes.section}>
                        <Grid item xs={12} className={classes.rowSection}>
                            <Tooltip title="Best Score so far in auto mode!" placement="top">
                                <Chip label={`Best: ${this.state.autoBest}`} color="primary" className={classes.chip} variant="outlined" />
                            </Tooltip>

                            <Tooltip title="Pokemon caught so far this run!" placement="top">
                                <Chip label={`Pokemon Caught: ${this.state.autoPokemonCaught}`} className={classes.chip} variant="outlined" />
                            </Tooltip>

                            <Tooltip title="Average Pokemon Spawned to get a shiny over all runs in auto mode" placement="top">
                                <Chip label={`Average: ${this.getAveragePokemonCaught(this.state.autoTrials) || 0}`} className={classes.chip} variant="outlined" />
                            </Tooltip>

                            <Tooltip title="Shiny chance, taking into considering lure, charm and catch combo" placement="top">
                                <Chip label={`Shiny Chance:  ${this.state.autoShinyChance}`} className={classes.chip} variant="outlined" />
                            </Tooltip>

                            <Tooltip title="Worst run so far, eg the most amount of pokemon it has taken." placement="top">
                                <Chip label={`Worst Run: ${this.state.autoWorst}`} color="secondary" className={classes.chip} variant="outlined" />
                            </Tooltip>
                        </Grid>

                        <Grid item xs={12} className={classes.graphHolder}>
                            <ColumnChart label="Pokemon Caught" id={'Pokemon Caught Until Shiny'} data={this.state.autoGraphData} min={0} max={null} legend={'bottom'}/>
                        </Grid>

                        {this.state.autoGotShiny && (
                            <Grid item xs={12}>
                                <Typography variant={'display3'} align={'center'}>
                                    Got Shiny!
                                </Typography>

                                <Typography variant={'title'} gutterBottom={true} align={'center'}>
                                    Caught after {this.state.autoShinyCaughtAt} pokemon spawned!
                                </Typography>
                            </Grid>
                        )}
                    </Grid>

                )}

                {/*Multi Session*/}
                {value === 2 && (
                    <Grid container alignContent={'center'} direction={'column'} className={classes.section}>

                        <Grid item xs={12} className={classes.rowSection}>

                            <Tooltip title="Best Score so far in multi mode!" placement="top">
                                <Chip label={`Best: ${this.state.leastCaught === 10000 ? 0 : this.state.leastCaught}`} color="primary" className={classes.chip} variant="outlined" />
                            </Tooltip>

                            <Tooltip title="Average amount of pokemon spawned to get a shiny over all runs, this session" placement="top">
                                <Chip label={`Average:  ${this.getAveragePokemonCaught(this.state.trials) || 0}`} className={classes.chip} variant="outlined" />
                            </Tooltip>

                            <Tooltip title="Worst run this session" placement="top">
                                <Chip label={`Worst: ${this.state.mostCaught}`} color="secondary" className={classes.chip} variant="outlined" />
                            </Tooltip>
                        </Grid>

                        <Grid item xs={12} className={classes.graphHolder}>
                            <ColumnChart label="Pokemon Caught" id={'Pokemon Caught Until Shiny'} data={this.state.graphData} min={0} max={null} legend={'bottom'}/>
                        </Grid>
                    </Grid>

                )}

                {/*Settings*/}
                {value === 3 && (
                    <Grid container alignContent={'center'} direction={'column'} className={classes.section}>

                        <Grid item xs={12}>
                            <Typography variant={'title'} gutterBottom={true} align={'center'}>
                                Global Settings
                            </Typography>

                            <Grid item xs={12} className={classes.toggleSection}>
                                <FormGroup row>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                id={'hasLure'}
                                                checked={this.state.hasLure}
                                                onChange={this.handleChange}
                                                value={this.state.hasLure}
                                            />
                                        }
                                        label="Have Lure?"
                                    />
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                id={'hasCharm'}
                                                checked={this.state.hasCharm}
                                                onChange={this.handleChange}
                                                value={this.state.hasCharm}
                                            />
                                        }
                                        label="Have Charm?"
                                    />
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                id={'disableCatchCombo'}
                                                checked={this.state.disableCatchCombo}
                                                onChange={this.handleChange}
                                                value={this.state.disableCatchCombo}
                                            />
                                        }
                                        label="Disable Catch Combo Rates"
                                    />
                                </FormGroup>
                            </Grid>


                        </Grid>

                        <Grid item xs={12}>
                            <Typography variant={'title'} gutterBottom={true} align={'center'}>
                                Multi Mode Settings
                            </Typography>

                            <FormGroup row className={classes.toggleSection}>

                                <FormControl className={classes.formControl} aria-describedby="trialCount">
                                    <InputLabel htmlFor="trialCount">How many Trials should we run?</InputLabel>
                                    <Input
                                        id="trialCount"
                                        type={'number'}
                                        value={this.state.trialCount}
                                        onChange={this.changeNumber}
                                    />
                                    <FormHelperText id="trialCount">Each trial will catch pokemon until it finds a shiny. (2-100)</FormHelperText>
                                </FormControl>
                            </FormGroup>
                        </Grid>
                    </Grid>
                )}

                {(value === 0 || value === 1 || value === 2) && (!this.state.manualGotShiny) && (

                    <Grid container alignContent={'center'} direction={'column'} className={classes.section}>
                        <Grid item xs={12} className={classes.pokeballHolder}>
                            <IconButton onClick={this.startSim} disabled={this.state.trialRunning}>
                                <img src={pokeball} alt={"catch it!"} className={classes.pokeball} />
                            </IconButton>
                        </Grid>
                    </Grid>

                )}

                <BottomNavigation
                    value={value}
                    onChange={this.handleNavChange}
                    showLabels
                    className={classes.footer}
                >
                    <Tooltip title="Hardcore Mode! 1 Pokemon Per Click" placement="top">
                        <BottomNavigationAction label="Manual Mode" className={classes.button} icon={<PanoramaFishEye />} />
                    </Tooltip>

                    <Tooltip title="Auto Mode, 1 Shiny Per Click and see how long it takes!" placement="top">
                        <BottomNavigationAction label="Auto Mode" className={classes.button} icon={<Timer />} />
                    </Tooltip>

                    <Tooltip title="Multi Mode, Run 100 simulations at once!" placement="top">
                        <BottomNavigationAction label="Multi Mode" className={classes.button} icon={<ScatterPlot />} />
                    </Tooltip>

                    <Tooltip title="Adjust your settings" placement="top">
                        <BottomNavigationAction label="Settings" className={classes.button} icon={<Settings />} />
                    </Tooltip>




                </BottomNavigation>



                {/*<AppBar position="static" className={classes.footer}>
                    <Toolbar className={classes.toolBar}>
                        <Typography variant="subheading" color="inherit" className={classes.grow}>
                            Created by <a href={'https://gitlab.com/users/Crumble/projects'}>Crumble</a>
                        </Typography>
                    </Toolbar>
                </AppBar>*/}

            </div>
        );
    }
}

export default withStyles(styles)(App);
