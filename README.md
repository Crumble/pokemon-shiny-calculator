[![pipeline status](https://gitlab.com/Crumble/pokemon-shiny-calculator/badges/master/pipeline.svg)](https://gitlab.com/Crumble/pokemon-shiny-calculator/commits/master)

Requires Node v8.4 at least. untested in earlier versions.

There has been lots of discussion over the odds of catching a shiny pokemon in Pokemon Let's Go.

I have seen lots of confusion over how it works, Some people angry that it took them over 1000 catches to get a shiny, Others boasting about catching one on their first attempt after 10 catches.

Well that is the beauty of RNG, So I decided to make a simple site that would demonstrate the odds.

You can start a single session, where the website will simulate catching pokemon until a shiny appears, Sometimes it will be 10 catches, Other times over 1000.

You can also batch sessions and run 100 simulations at once and view the averages, best runs and longest runs on a graph.

You can also choose to activate Shiny Charm or Lure and see how it affects your chances.

I hope someone finds this interesting or useful.

You can view the website [here](https://crumble.gitlab.io/pokemon-shiny-calculator/)

The code is open source, I am open to any improvements. At the moment it uses pseudo random generation which is what i believe Pokemon Let's Go uses. 

Feel free to open a pull request if you have any improvements on either design or the js code.

How to use.

1. Clone the repo.
2. run yarn inside the project directory
3. run yarn start
4. that's it! Open your browser to localhost:3000

